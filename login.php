<?php
session_name("jackzmcSocial");
session_start();
?> 
 <!doctype html>
<html>
<head>
	<meta charset="utf-8">
	
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<link rel="stylesheet" href="css/foundation.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
	<link rel="icon" href="/favicon.png">
	<link href='https://fonts.googleapis.com/css?family=Fontdiner+Swanky' rel='stylesheet' type='text/css'>
	<script src="js/vendor/modernizr.js"></script>
	
	<style>
	/* #login {
		display: block;
		position: relative;
		left: 25%;
		top: 25%;
	}
	#register { 
		display: block;
		position: relative;
		left: 25%;
		top: 25%;
	} */
	</style>
	<title id='titleTop'>Profile | Social</title>
</head>
<?php
include("config.php");
if ($config_active !== true) {
	die("<strong>The config could not be loaded and as a fallback this page is disabled. <br>Please contact the admins at me@jackzmc.me</strong>");
}
if ($_SESSION["state"] == "true") {

	header("Location: ../social");
}else{
	echo "<script>window.onload = function() {
	document.getElementById('loginButton').style.display = 'block';
	document.getElementById('registerButton').style.display = 'block';
	}</script>";
}

?>
<body>
<div class="top-bar">
  <div class="top-bar-left">
    <ul class="dropdown menu" data-dropdown-menu>
      <li class="menu-text">Social</li>
      <li>
        <a href="../social">Home</a>
      </li>
	  <li><a href='members.php'>Members</a></li>
	  <li><a href='rules.php'>Rules</a></li>
	  <li><a href='contact.php'>Support</a></li>


    </ul>
  </div>
  <div class="top-bar-right">
    <ul class="menu">
		<li><a style="display:none" id='loginButton' href="login.php?mode=login">Login</a></li>
		<li><a style="display:none" id='registerButton' href="login.php?mode=register">Register</a></li>
    </ul>
  </div>
</div>
<br>
<br>
<div class="row">
		<div id='LoginDisabled' style="display:none" class="large-12 callout alert">
			<h1>Login & Registering has been disabled.</h1>
			<p>Login/Registering was disabled by a config file, sorry :(. This may be due to an security incident or some type of breach or even regular maintenance</p>
		</div>

		
		<div id='main' class="callout text-center">
			<div  style="" id='login' class="medium-offset-3 medium-6 ">
				<h1>Login</h1>
				<p>PLEASE DO NOT USE REAL PASSWORDS, THIS IS NOT 100% SECURE. Currently method: salt & password_hash. 2FA NOT SETUP YET!<a href="?mode=register">Don't have an account?</a></p>
				<form method="POST" action="validate.php">
				<!-- form will be POST to home -->
				<label>Username</label>
				<input placeholder="Username [no email]" type="text" name="usernameIn" id="usernameIn" />
				<label>Password</label>
				<input placeholder="Password" type="password" name="passwordIn" id="passwordIn" />
				<input class="button" value="Login" type="submit" name="submit" id="submit" />
				</form>
			</div>
			<div style="" id='register' class="medium-offset-3 medium-6">
				<h1>Register</h1>
				<p>PLEASE DO NOT USE REAL PASSWORDS, THIS IS NOT 100% SECURE. Currently method: salt & password_hash. 2FA NOT SETUP YET!<a href="?mode=login">Already have an account?</a></p>
				<p>Your avatar will be using gravatar.com and with your email address. If no icon is set, it will be defaulted</p>
				<form method="POST" action="validate.php">
				<!-- form will be POST to home -->
				<label>Username</label>
				<input placeholder="Username" required type="text" name="usernameIn" id="usernameIn" />
				<label>Password</label>
				<input placeholder="Password (GREATER THEN 8 CHARACTERS)" min="8" required type="password" name="password" id="passwordIn" />
				<label>Confirm password</label>
				<input placeholder="Confirm password"  required type="password" name="passwordConfirm" id="passwordConfirm" />
				<label>Full name</label>
				<input placeholder="Full Name"  required type="text" name="name" id="name" />
				<label>Phone number (000-000-0000 format)</label>
				<input placeholder="Phone number (000-000-0000 format)" required type="text" name="phone" id="phone" />
				<label>Valid email address</label>
				<input placeholder="Valid Email Address"  required type="email" name="email" id="email" />
				<label>Confirm email address</label>
				<input placeholder="Cofirm Email"  required type="email" name="emailConfirm" id="emailConfirm" />
				<input type="checkbox" name="terms" id="terms" required /> I Agree to <a target='_blank' href="../social/termsofservice.php">Terms of Service</a><br>
				<input class="button" value="Register"  type="submit" name="submit" id="submit" />
				</form>
			</div>
		</div>

	<p class="text-center">Copyright © <a href="https://jackzmc.me">Jackz Minecraft</a>&nbsp;&nbsp;&nbsp;<a href="termsofservice.php">Terms of Service</a></p>
</div>
<script src="js/vendor/jquery.js"></script>
<?php
$mode = $_GET["mode"];
if ($loginRegister == true) {
	 if ($mode == "login") {
		echo "<script>document.getElementById('register').style.display = 'none';document.getElementById('login').style.display = 'block';</script>";
	}
	else if ($mode == "register") {
		echo "<script>document.getElementById('login').style.display = 'none';document.getElementById('register').style.display = 'block';</script>";

	}else{

		echo "<script>document.getElementById('register').style.display = 'none';document.getElementById('login').style.display = 'block';</script>";

		//maybe do something
	} 
}else{
	echo "<script>document.getElementById('main').style.display = 'none';document.getElementById('LoginDisabled').style.display = 'block';</script>";
}
?>

<script src="js/vendor/fastclick.js"></script>
<script src="js/foundation.min.js"></script>
<script>$(document).foundation();</script>
</body>
</html>