<?php
session_name("jackzmcSocial");
session_start();
$f3 = require('fatfree/lib/base.php');
require("config.php");
$db=new \DB\SQL('mysql:host=localhost;port=3306;dbname=' . $db_name,$db_user,$db_pass);
//error_reporting(-1);
include('swear.php');
		
if ($_SESSION["state"] == "true") {
	$rows=$db->exec("SELECT activated FROM `social_Users` WHERE username='" . $_SESSION['user'] . "'");
	if (count($rows) > 0) {
			foreach($rows as $row)
			if ($row['activated'] == 1) {
				$activated = true;
			}else{
				$activated = false;
			}
	}else{
		die("Sorry but we failed to detect if your account is activated.");
	}
	echo "<script>window.onload = function() {
		document.getElementById('profileWelcome').innerHTML = '" . $_SESSION["user"] . "';
		document.getElementById(\"profileButton\").style.display = 'block'
		document.getElementById('loginButton').style.display = 'none';
	document.getElementById('registerButton').style.display = 'none';
	document.getElementById('welcome').style.display = 'block';
	document.getElementById('welcome-user').innerHTML = '" . $_SESSION["user"] . "'
	document.getElementById('welcome_noLogin').style.display = 'none';
	document.getElementById('report_signin').style.display = 'block';
	}</script>";
	
}else{
		$activated = false;
	echo "<script>window.onload = function() {
		document.getElementById(\"profileButton\").style.display = 'none'
	document.getElementById('loginButton').style.display = 'block';
	document.getElementById('registerButton').style.display = 'block';
	document.getElementById('welcome_noLogin').style.display = 'block';
	document.getElementById('report_tologin').style.display = 'block';
	}</script>";
	
	//die("Test");
}
?>

 <!doctype html>
<html>
<head>
	<meta charset="utf-8">
	
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<link rel="stylesheet" href="css/foundation.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
	<link rel="icon" href="/favicon.png">
	<link href='https://fonts.googleapis.com/css?family=Fontdiner+Swanky' rel='stylesheet' type='text/css'>
	<script src="js/vendor/modernizr.js"></script>
	<style>
	.creator {
		position: absolute;
		display: inline-block;
		top:10px;
	}
	.message {
		position: relative;
		top: 5px;
	}
	.report {
		position: absolute;
		color: red;
		right:5px;
		bottom:5px;
	}
	.report:hover {
		color: darkred;
	}
	.like {
		position: absolute;
		color: #00a3cc;
		right:70px;
		bottom:7px;
	}
	.liked {
		position: absolute;
		color: blue;
		right:70px;
		bottom:7px;
	}
	.liked:hover {
		
		color: blue;

	}
	</style>
	<title id='titleTop'>Social | Jackz Code</title>
</head>

<body>
<div class="reveal" id="sendMessage" data-reveal>
	<h1>Compose message</h1>
	<p>Please respect the rules before posting or it might result in account termination. <a target="_Blank" href="rules.php">Open Rules (New Tab)</a></p>
	<?php
	if ($activated == true) {
	echo "
	<form method=\"post\" action=\"msgSend.php\">
	<textarea type='text' name=\"composeText\" id='composeText' placeholder='Put your message here' ></textarea>

	<input type=\"submit\" class='button' name=\"submit\" id='submit' value=\"Compose\" />
	</form>";
	}else{
		echo "Sorry but you must have an activated account.";
	}
	
	?>
  <button class="close-button" data-close aria-label="Close modal" type="button">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
<div class="reveal" id="report" data-reveal>
	<span style="display:none" id='report_signin'>
	<h1>Report a post</h1>
	<p>You clicked to report Post #<span id='report_id'></span>. Please specify the message's content and why you have flagged it.<p>
	<form method="POST" action="flag.php">
		<label>Username</label><input type="text" id='username' DISABLED name='username' value='<?php echo $_SESSION["user"]; ?>'>
		<input type="hidden" id="messageID" name="messageID" value="" />
		<label>Message content</label><textarea placeholder="Please enter the message's content (what did it say)" id='content' name='content'></textarea>
		<label>Reasoning</label><textarea style="height:188px;" placeholder="Why did you want to flag this message? Specify here." id='reason' name='reason'></textarea>
		<input type="submit" class="button text-center" id="submit" name="submit" value="Report Message" />
	</form>
	<!-- MESSAGE HERE -->
	<!-- form here asking why -->
	<!--<form method="post" action="msgSend.php">
	<textarea type='text' name="composeText" id='composeText' placeholder='Put your message here' ></textarea>

	<input type="submit" class='button' name="submit" id='submit' value="Compose" />
	</form>-->
	</span>
	<span style="display:none" id='report_tologin'><h1>Report a post</h1><p>You need to be signed in to report a post. Sign in below: <br>
	<iframe height="365px" width="100%" frameborder="0" src='login_box.php?mode=login' ></iframe></span>
  <button class="close-button" data-close aria-label="Close modal" type="button">
    <span aria-hidden="true">&times;</span>
  </button>
</div>


<div class="top-bar">
  <div class="top-bar-left">
    <ul class="dropdown menu" data-dropdown-menu>
      <li class="menu-text">Social</li>
      <li class='active'>
        <a href="#">Home</a>
      </li>
	  <li><a href='members.php'>Members</a></li>
	  <li><a href='rules.php'>Rules</a></li>
	  <li><a href='contact.php'>Support</a></li>

    </ul>
  </div>
  <div class="top-bar-right">
    <ul class="menu">
		<li><a style="display:none" id='profileButton' class="LoggedIn" href="profile.php"><i class="fa fa-user" aria-hidden="true"></i> <span id='profileWelcome'></span></a></li>
		<li><a style="display:none" id='loginButton' href="login.php?mode=login">Login</a></li>
		<li><a style="display:none" id='registerButton' href="login.php?mode=register">Register</a></li>

    </ul>
  </div>
</div>
<br>
<br>
<div class="row">
	
	<div style="display:none" id='welcome' class="large-12  text-center">
		<h1>Hello <span id='welcome-user'></span></h1>
		<p>Logout, and manage settings by pressing your name in the top right corner</p>
		<p>Swear filter is enabled by default if you are logged in or newly registered!</p>
		<a data-open="sendMessage" class="button success">Send a message</a>
	</div>
	<div style="display:none" id='welcome_noLogin' class="large-12  text-center">
		<h1>Please login</h1>
		<p>Swear filter is enabled by default if you are logged in or newly registered!</p>
		<!--<div class="callout alert"><strong><u>Notice</u>: If you see 'Internal Server Error' below, this is a known problem. If you login or register the problem should not happen. This only affects non-logged in users. Sorry! </strong></div>-->
	</div>
	<hr>
		<?php
		
		$contents=$db->exec("SELECT * FROM `social_content`");
		if (count($contents) > 0) {
			foreach($contents as $content) {
				$msg = $content['message'];
				$msg = html_entity_decode($msg,ENT_QUOTES);
				$msgCreator = $db->exec("SELECT email,rank FROM `social_users` WHERE username='" . $content['creator'] . "'");
				if(count($msgCreator) > 0) {
					foreach($msgCreator as $msgcre)
					$avatarEmail = md5(strtolower(trim($msgcre['email'])));
					$url = "https://www.gravatar.com/avatar/" . $avatarEmail . "?d=" . $defaultAvatar . "&r=pg&s=200";
				
					if ($msgcre['rank'] == "admin") {
						$atname = $content['creator'] . "<a style='color:gold' title='Administrator' onClick=\"alert('This user is an admin.');\"> <i  class=\"fa fa-star\" aria-hidden=\"true\"></i></a>";
					}else{
						$atname = $content['creator'];
					}
				}else{
					die("An database error happened, oops!");
				}
				if ($_SESSION['state'] == true) {
					$filters=$db->exec("SELECT `swearFilter` FROM `social_users` WHERE username='" . $_SESSION['user'] . "'");
					foreach($filters as $filter) 
					if ($filter['swearFilter'] == 1) {
						$msg = str_ireplace($swear, "**** ", $msg);
					}//value (1 / 0)
					
				}else{
					//Filter: active
					$msg = str_ireplace($swear, "**** ", $msg);
				}
				if ($_SESSION['state'] == true) {
					$like = "<a id='like" . $content['id'] . "' class='like' title='Like this post' onClick='like(" . $content['id'] . ")'><i class=\"fa fa-thumbs-up\" aria-hidden=\"true\"></i> <span id='likeCount" . $content['id'] . "'>" . $content['likes'] . "</span></a>";
				}else{
					$like = "";
				}
				echo "<div class='large-12 callout'><a href='user.php?user=" . $content['creator'] . "'><img width='75px' height='75px' src='" . $url . "' /></a>&nbsp;&nbsp;<h4 class='creator'>By <a href='user.php?user=" . $content['creator'] . "'>@" . $atname . "</a> on " . $content['posted'] . "</h4><span class='message'>" . $msg . "</span>" . $like . " <a class='report' data-open='report' title='Report this post' onClick='report(" . $content['id'] . " )'><i class=\"fa fa-flag\" aria-hidden=\"true\"></i> Flag</a></div>";
				//unset($atname);
			}
		}else{
			echo "<div class='callout alert text-center'><strong>Failed to grab content. Database returned nothing, sorry!</strong></div>";
		}
		?>


	<hr>
	<div class="large-12 callout secondary ">
		<h4 class="text-center">Idea List</h4>
		<?php include('ideas.html') ?>
	</div>
	<p class="text-center">Copyright © <a href="https://jackzmc.me">Jackz Minecraft</a>&nbsp;&nbsp;&nbsp;<a href="termsofservice.php">Terms of Service</a></p>
</div>

<script src="js/vendor/jquery.js"></script>
<script src="js/vendor/fastclick.js"></script>
<script src="js/foundation.min.js"></script>
<script>
$(document).foundation();
function report(id) {
	document.getElementById('report_id').innerHTML = id;
	document.getElementById('messageID').value = id;
	//alert("Not available");
}
function like(id) {
	
			// document.getElementById('likeCount' + id).innerHTML = parseInt(document.getElementById('likeCount' + id).innerHTML) + 1; //to remove
	$(function(){
       $.get("likePost.php?id=" + id + "&user=<?php echo $_SESSION['user']; ?>", function(data){
			//console.log("LikePost Response: " + data);
			
			var res = data.split("|");
			if (res[0] == "Success") {
				console.log('Like Post #' + res[2] + ', was Success');
				document.getElementById('like' + id).className = "liked";
				document.getElementById('likeCount' + id).innerHTML = res[1];
			}
       });
    });
	
	
	//console.log("Liked post #" + id) 
}

</script>
</body>
</html>