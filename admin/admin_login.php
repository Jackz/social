<?php
session_name("jackzmcSocial");
session_start();
?> 
 <!doctype html>
<html>
<head>
	<meta charset="utf-8">
	
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<link rel="stylesheet" href="css/foundation.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
	<link rel="icon" href="/favicon.png">
	<link href='https://fonts.googleapis.com/css?family=Fontdiner+Swanky' rel='stylesheet' type='text/css'>
	<script src="js/vendor/modernizr.js"></script>
	
	<style>

	</style>
	<title id='titleTop'>Admin Login</title>
</head>
<?php
if ($_SESSION['AdminChecked'] == true) {
	header("Location: admin.php");
	die();
}
$user = $_POST['usernameIn'];
$pass = $_POST['passwordIn'];
$submit = $_POST['submit'];
if(isset($_POST['passwordIn']) == true && isset($user) && isset($submit)) {
	//do later
	$_SESSION['AdminChecked'] == true;
	header("location: admin.php");
}else{
	?>
	<script>document.getElementById('alert').innerHTML = "Username or password is invalid";document.getElementById('alert').style.display = 'block';</script>
	<?php
}
include("config.php");
if ($config_active !== true) {
	die("<strong>The config could not be loaded and as a fallback this page is disabled. <br>Please contact the admins at me@jackzmc.me</strong>");
}
if ($_SESSION["state"] == "true") {

	header("Location: ../social");
}
?>
<body>

<div class="row">
		<div id='LoginDisabled' style="display:none" class="large-12 callout alert">
			<h1>Login & Registering has been disabled.</h1>
			<p>Login/Registering was disabled by a config file, sorry :(. This may be due to an security incident or some type of breach or even regular maintenance</p>
		</div>

		
	<div id='main' class="callout text-center">
			<div  style="" id='login' class="medium-offset-3 medium-6 ">
				<h1>Re Login</h1>
				<p>To login into admin panel, please relogin to confirm your information.</p>
				<span id='alert' style="display:none" class="callout alert"></span>
				<form method="POST" action="admin_login.php">
				<!-- form will be POST to home -->
				<label>Username</label>
				<input placeholder="Username [no email] (case sensitive for now)" type="text" name="usernameIn" id="usernameIn" />
				<label>Password</label>
				<input placeholder="Password" type="password" name="passwordIn" id="passwordIn" />
				<input class="button" value="Login" type="submit" name="submit" id="submit" />
				</form>
			</div>

	<p class="text-center">Copyright � <a href="https://jackzmc.me">Jackz Minecraft</a>&nbsp;&nbsp;&nbsp;<a href="termsofservice.php">Terms of Service</a></p>
</div>
<script src="js/vendor/jquery.js"></script>


<script src="js/vendor/fastclick.js"></script>
<script src="js/foundation.min.js"></script>
<script>$(document).foundation();</script>
</body>
</html>