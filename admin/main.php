﻿<?php
session_name("jackzmcSocial");
session_start();
include("../config.php");
$f3 = require('../fatfree/lib/base.php');
$db=new \DB\SQL('mysql:host=localhost;port=3306;dbname=' . $db_name,$db_user,$db_pass);
if ($config_active !== true) {
	die("<strong>The config could not be loaded and as a fallback this page is disabled. <br>Please contact the admins at me@jackzmc.me</strong>");
}
if ($_SESSION["state"] == "true") {

	echo "<script>window.onload = function() {
		document.getElementById(\"profileWelcome\").innerHTML = '" . $_SESSION["user"] . "';

	}</script>";
	$users=$db->exec("SELECT id,rank,username,email,fullname,realLastLogin FROM `social_Users` WHERE username='" . $_SESSION['user'] . "'");
			if (count($users) > 0) {
				foreach($users as $user)
					if ($user['rank'] == "admin") {
					}else{
						header("Location: profile.php");
						die("Not admin");
					}

			}else{
				die("<strong>An fatal error occurred. We could not detect if you were an admin. </strong");
			}
}else{
	header("Location: ../login.php?mode=login");
}

?>
<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<link rel="stylesheet" href="../css/foundation.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
	<link rel="icon" href="/favicon.png">
	<link href='https://fonts.googleapis.com/css?family=Fontdiner+Swanky' rel='stylesheet' type='text/css'>
	<script src="../js/vendor/modernizr.js"></script>
	<style>
	.LoggedIn {
		display: none;
	}
	.LoggedIn-Hide {
		display: none;
	}
	</style>
	<title id='titleTop'>Admin | Social</title>
</head>
<div class="reveal" id="changeUsername" data-reveal>
  <h1>Change username</h1>
  <p><strong>You selected to change username: </strong><span id='changeuser_username'></span></p>
  <form method="post" action="changeuser.php">
  <input readonly type="text" id="username" name="username" placeholder="Error" />
  <input type="text" id="newUser" name="newUser" placeholder="New username (ALL LOWERCASE, FIRST LETTER CAPPED)">
  <input class="button" type="submit" id="submit" name="submit" value="Confirm">
  </form>
  <button class="close-button" data-close aria-label="Close modal" type="button">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
<div class="reveal" id="changeName" data-reveal>
	<h1>Change full name</h1>
  <p><strong>You selected to change full name for: </strong><span id='changename_name'></span></p>
  <p><strong>Previous name: </strong><span id='prevname'></span></p>
  <form method="post" action="changename.php">
  <input readonly type="text" id="username1" name="username1" placeholder="Error" />
  <input type="text" id="name" name="name" placeholder="New first & last name">
  <input class="button" type="submit" id="submit" name="submit" value="Confirm">
  </form>
  <button class="close-button" data-close aria-label="Close modal" type="button">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
<div class="reveal" id="userReset" data-reveal>

	<h1>Reset password</h1>
	<p><b>Do you confirm to reset</b> <span id='resetPass_name'></span><b>'s password?</b> They will be forced to password reset to login.</b>
	<form method="post" action="resetpass.php">
	<input readonly type="text" id="username2" name="username2" placeholder="Error" />
	<input class="button" type="submit" id="submit" name="submit" value="Confirm">
	</form>
	
  <button class="close-button" data-close aria-label="Close modal" type="button">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
<div class="reveal" id="changeEmail" data-reveal>
	<h1>Change email</h1>
  <p><strong>You selected to change email for: </strong><span id='changeEmail_name'></span></p>
  <p><strong>Previous email: </strong><span id='prevEmail'></span></p>
  <form method="post" action="emailChange.php">
 <input readonly type="text" id="username3" name="username3" placeholder="Error" />
  <input type="text" id="newemail" name="newemail" placeholder="New email">
  <input class="button" type="submit" id="submit" name="submit" value="Confirm">
  </form>
	
	
  <button class="close-button" data-close aria-label="Close modal" type="button">
    <span aria-hidden="true">&times;</span>
  </button>
</div>

<body>

<div class="top-bar">
  <div class="top-bar-left">
    <ul class="dropdown menu" data-dropdown-menu>
      <li class="menu-text">Social</li>
      <li>
        <a href="../">Home</a>
      </li>
	  <li><a href='../members.php'>Members</a></li>
	  <li><a href='../rules.php'>Rules</a></li>
	  <li><a href='../contact.php'>Support</a></li>
	  <li class="active"><a href='admin.php'>Admin</a></li>
    </ul>
  </div>
  <div class="top-bar-right">
    <ul class="menu">
		<li><a  id='profileButton' class="LoggedIn" href="../profile.php"><i class="fa fa-user" aria-hidden="true"></i> <span id='profileWelcome'></span></a></li>

    </ul>
  </div>
</div>
<br>
<br>
<div class="row " data-equalizer>
	<h1 class="text-center">Admin Panel</h1>
	<p>Hello admin, this is the admin panel. Manage user's settings from here or other settings for this site. Disabling the whole site or database-updating cannot be disabled from here, only from a config file due to it disabling the ability to login. Below you can manage: User's name, email visibility, email, and reset their password</p>
	<p>Click (change) or (toggle) to change the user's setting. Reset password will set it to zero, forcing user to reset. Below the settings is the user list.</p>
	<h3>Your last login: <?php echo $user['realLastLogin']; ?></h3>
	<hr>
	<h3 class="text-center subheader">Website Settings</h3>
	<!--<p class="text-center">No settings were found</p>-->
		<div  class="switch">
		<label>Database Update Toggle (User Login/Settings)</label>
		 <input class="switch-input" type="checkbox" checked name="dbUpdates" id="dbUpdates" onchange="settings_dbupdate();" >
		  <label class="switch-paddle" for="dbUpdates">
			<span class="show-for-sr">Database Toggle</span>
			<span class="switch-active" aria-hidden="true">On</span>
			<span class="switch-inactive" aria-hidden="true">Off</span>
		  </label>
		</div>		
	<hr>
	<h3 class="text-center subheader">User Accounts</h3>
	<table>
	<thead>
		<tr>
			<th width="2px"></th>
			<th style="width:10px">Username</th>
			<th>Full Name</th>
			<th width="30px">Password</th>
			<th width="40px">Email</th>
			<th width="10px">Email Visible</th>
			<th>Swear Filter</th>
			<th>Activated</th>
			
		</tr>
	</thead>
	<tbody>
	<!-- WILL PROBABLY AJAX TO CHANGE? -->
	<?php
	$rows=$db->exec("SELECT rank,ID,email,EmailVisible,LastLogin,username,fullname,swearFilter,activated FROM `SOCIAL_users`");
			if (count($rows) > 0) {
			foreach($rows as $row) {
					$email = "<a href='mailto:" . $row['email'] . "'>" . $row['email'] . "</a>";
					$email = "<a href='mailto:" . $row['email'] . "'>" . $row['email'] . "</a>";

				if ($row['rank'] == "admin") {
					$prefix = "<a style='color:gold' title='Administrator' onClick=\"alert('This user is an admin.');\"> <i  class=\"fa fa-star\" aria-hidden=\"true\"></i></a> ";
				}else{
					$prefix = "";
				}
				if ($row['activated'] == 1) {
					$active = "Activated";;
				}else{
					$active = "<a  onclick=\"activate('" . $row['username'] . "')\" class='button success'>Activate</a>";
				}
				$pass2=$db->exec("SELECT `password` FROM `SOCIAL_users` WHERE username='" . $row['username'] . "'");
				if (count($pass2) > 0) {
					foreach($pass2 as $pass) 
						//echo "pass: " . $pass['password'] . "<br>";
						if (empty($pass['password']) == true) {
							$passReset = "Already Reset";
							unset($pass);
						}else{
							$passReset = "<a data-open='userReset' onclick=\"userReset('" . $row['username'] . "')\" class='button'>Reset</a>";
							unset($pass);
						}
					
				}else{
					$passReset = "Failed";
				}
				
			
				$emailVis = ($row['EmailVisible'] == 1) ? 'Yes' : 'No';
				//$toChange = ($row['EmailVisible'] == 0) ? true : false;
				$swear = ($row['swearFilter'] == 1) ? 'Active' : 'Deactivated';

				$user = "<a href='../user.php?user=" . $row['username'] . "'>" . $row['username'] . "</a>";
				
				//rank
				echo "<tr><td>" . $prefix . "</td>"; 
				//username
				echo "<td>" . $user . " (<a data-open='changeUsername' onClick=\"changeUsername('" . $row['username'] . "')\">change</a>)</td>";
				//full name
				echo "<td>" . $row['fullname'] . " (<a data-open='changeName' onClick=\"changeName('" . $row['username'] . "','" . $row['fullname'] . "')\">change</a>)</td>";
				//Password
				echo "<td>" . $passReset . "</td>";
				//email
				echo "<td>" . $email . " (<a data-open='changeEmail' onClick=\"changeEmail('" . $row['username'] . "','" . $row['email'] ."')\">change</a>)</td>";
				//Email Visibility
				echo "<td class='text-center'>" . $emailVis ."</span> (<a href='emailvisToggle.php?user=" . $row['username'] . "&val=" . $row['EmailVisible'] . "'>toggle</a>)</td>";
				//swear filter
				//echo "<td>" . $swear . " (<a onClick=\"swearfilter('" . $row['username'] . "'," . $swear . ")\">toggle</a>)</td>";
				echo "<td class='text-center'>" . $swear ."</span> (<a href='sweartoggle.php?user=" . $row['username'] . "&val=" . $row['swearFilter'] . "'>toggle</a>)</td>";
				echo "<td>" . $active . "</td>";
				echo "</tr>";
				unset($passReset);

			}
			}else{
				die("A fatal error occurred and the username list could not be listed");
			}
	
	?>
	</tbody>
	</table>
</div>

	<p class="text-center">Copyright © <a href="https://jackzmc.me">Jackz Minecraft</a>&nbsp;&nbsp;&nbsp;<a href="termsofservice.php">Terms of Service</a></p>
	<!--<Div class="row">
	<div class="large-12 callout secondary text-center">
		Copyright © <a href="https://jackzmc.me">Jackz Minecraft</a>  <!-- remove br and space when ad --> <br>&nbsp;
		<!-- insert ad here -->
	<!--
	</div>
</div>-->

<script src="../js/vendor/jquery.js"></script>
<script src="../js/vendor/fastclick.js"></script>
<script src="../js/foundation.min.js"></script>
<script>
$(document).foundation();
function changeUsername(user) {
	document.getElementById("changeuser_username").innerHTML = user;
	document.getElementById("username").value = user;
}
function changeName(user, name) {
	document.getElementById("username1").value = user;
	document.getElementById("changename_name").innerHTML = user;
	document.getElementById("prevname").innerHTML = name;
}
function userReset(user) {
	document.getElementById("username2").value = user;
	document.getElementById("resetPass_name").innerHTML = user;
}
/*function emailVisToggle(user,currentValue, id) {
		if (currentValue == "yes") {
		currentValue = true;
	}else{
		currentValue = false;
	}
	$(function(){
       $.get("emailvisToggle.php?user=" + user + "&id=" + id + "&val=" + currentValue, function(data){
			//console.log("LikePost Response: " + data);
			console.log(data);
			var res = data.split("|");
			if (res[0] == "Success") {
				if (res[1] == true) { var output = "Yes"; }else{ var output = "No"; }
				document.getElementById('emailVis' + id).innerHTML = output;
			}
       });
    });
}*/

function swearfilter(user,currentValue) {
	if (currentValue == "yes") {
		currentValue = true;
	}else{
		currentValue = false;
	}
	$(function(){
       $.get("emailvisToggle.php?user=" + user + "&val=" + currentValue, function(data){
			//console.log("LikePost Response: " + data);
			
			var res = data.split("|");
			if (res[0] == "Success") {
				//console.log('Like Post #' + res[2] + ', was Success');
				document.getElementById('like' + id).className = "liked";
				
				document.getElementById('likeCount' + id).innerHTML = res[1];
			}
       });
    });
}
function changeEmail(user, email) {
	document.getElementById("username3").value = user;
	document.getElementById("changeEmail_name").innerHTML = user;
	document.getElementById("prevEmail").innerHTML = email;
}
function activate(user) {
	alert("Not feature, contact jackz");
}

</script>
</body>
</html>