<?php
session_name("jackzmcSocial");
session_start();
?> 
 <!doctype html>
<html>
<head>
	<meta charset="utf-8">
	
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<link rel="stylesheet" href="css/foundation.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
	<link rel="icon" href="/favicon.png">
	<link href='https://fonts.googleapis.com/css?family=Fontdiner+Swanky' rel='stylesheet' type='text/css'>
	<script src="js/vendor/modernizr.js"></script>
	<style>
	.LoggedIn {
		display: none;
	}
	.LoggedIn-Hide {
		display: none;
	}
	.EmailHid {
		color: red;
	}
	</style>
	<title id='titleTop'>Support | Social</title>
</head>
<?php
/*$_SESSION["state"] = $_GET["loggedin"];
$_SESSION["user"] = $_GET["user"];
if ($_SESSION["state"] !== "true" && $_SESSION["state"] !== "false") {
	header("Refresh:0; url=?loggedin=false");
}
if (empty($_SESSION["user"]) == true) {
	$_SESSION["user"] = -1;
}

if ($_SESSION["state"] == "true") {
	if ($_SESSION["user"] == "-1") {
		//$_SESSION["state"] = "false";
		header("Refresh:0; url=?loggedin=false&user=");
	}
}
*/
/* dev code */
//this code will autologin if username set, state equal false
/*
else{
	if ($_SESSION["user"] !== -1) {
		header("Refresh:0; url=?loggedin=true&user=" . $_SESSION["user"]);
	}
}
*/
/* end dev code */
include("config.php");
$f3 = require('fatfree/lib/base.php');
$db=new \DB\SQL('mysql:host=localhost;port=3306;dbname=' . $db_name,$db_user,$db_pass);
if ($_SESSION["state"] == "true") {

	echo "<script>window.onload = function() {
		document.getElementById(\"profileWelcome\").innerHTML = '" . $_SESSION["user"] . "';
		document.getElementById(\"profileButton\").style.display = 'block'
		document.getElementById('loginButton').style.display = 'none';
	document.getElementById('registerButton').style.display = 'none';
	}</script>";
}else{
	echo "<script>window.onload = function() {
		document.getElementById(\"profileButton\").style.display = 'none'
	document.getElementById('loginButton').style.display = 'block';
	document.getElementById('registerButton').style.display = 'block';
	}</script>";
}
?>
<body>
<div class="top-bar">
  <div class="top-bar-left">
    <ul class="dropdown menu" data-dropdown-menu>
      <li class="menu-text">Social</li>
      <li>
        <a href="../social">Home</a>
      </li>
	  <li ><a href='members.php'>Members</a></li>
	  <li><a href='rules.php'>Rules</a></li>
	  <li class='active'><a href='contact.php'>Support</a></li>
    </ul>
  </div>
  <div class="top-bar-right">
    <ul class="menu">
		<li><a style="display:none" id='profileButton' class="LoggedIn" href="profile.php"><i class="fa fa-user" aria-hidden="true"></i> <span id='profileWelcome'></span></a></li>
		<li><a style="display:none" id='loginButton' href="login.php?mode=login">Login</a></li>
		<li><a style="display:none" id='registerButton' href="login.php?mode=register">Register</a></li>

    </ul>
  </div>
</div>
<br>
<br>
<div class="row">
	<div  id='welcome' class="large-12 callout text-center">
	<h1 class="text-center">Contact / Support</h1>
	<p class="text-center">If you need help with this website send us an email! Email: <a href="mailto:me@jackzmc.me">me@jackzmc.me</a> or send using this form</p>
	<!--<form method="GET" action="contactSend.php">
	<input style="width:20%;display:inline;" type='text' placeholder="Your name" id='name' name='name' />
	<textarea  placeholder="Message" id="msg" name="msg" ></textarea><br>
	<input type="submit" value="Send" class="button" />
	</form>-->
	<h4>Please email us: me@jackzmc.me</h4>
	</div>
	<p class="text-center">Copyright © <a href="https://jackzmc.me">Jackz Minecraft</a>&nbsp;&nbsp;&nbsp;<a href="termsofservice.php">Terms of Service</a></p>
</div>

<script src="js/vendor/jquery.js"></script>
<script src="js/vendor/fastclick.js"></script>
<script src="js/foundation.min.js"></script>

</body>
</html>