<?php
session_name("jackzmcSocial");
session_start();
include("config.php");
$f3 = require('fatfree/lib/base.php');
$db=new \DB\SQL('mysql:host=localhost;port=3306;dbname=' . $db_name,$db_user,$db_pass);

$manualReset = $_GET['manual'];
if($manualReset == true) {
	//Password is "";
	?>
	<script>document.getElementById("currentPass").style.display = "none";</script>
	<?php
}
?> 
 <!doctype html>
<html>
<head>
	<meta charset="utf-8">
	
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<link rel="stylesheet" href="css/foundation.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
	<link rel="icon" href="/favicon.png">
	<link href='https://fonts.googleapis.com/css?family=Fontdiner+Swanky' rel='stylesheet' type='text/css'>
	<script src="js/vendor/modernizr.js"></script>
	<title id='titleTop'>Reset Password | SocialS</title>
</head>
<body>
<br>
<br>
<div class="row">
	<div id='main' class="callout text-center">
		<div  style="" id='login' class="medium-offset-3 medium-6 ">
			<h1>Reset password</h1>
			<p>Reset your password or change password if you forgot it, or it was reset manually</p>
			<form method="post" action="formReset.php">
			<div class="currentPass">
			<label>Current Password </label>
			<input type="password" id="oldpass" name="oldpass" placeholder="Your current password">
			</div>
			<label>New password (8 chars+ w/ caps)</label>
			<input required type="password" id="mainpass" name="mainpass" placeholder="New password">
			<label>Confirm password</label>
			<input required type="password" id="newPass" name="newPass" placeholder="Confirm password">
			<input type="submit" id="submit" class="button success">
			</form>
			
		
		</div>
	</div>
</div>	
	<p class="text-center">Copyright © <a href="https://jackzmc.me">Jackz Minecraft</a>&nbsp;&nbsp;&nbsp;<a href="termsofservice.php">Terms of Service</a></p>

<script>
<?php 
?>




</script>

<script src="js/vendor/jquery.js"></script>
<script src="js/vendor/fastclick.js"></script>
<script src="js/foundation.min.js"></script>

</body>
</html>