<?php
session_id("jackzmcSocial");
session_start();
?> 
 <!doctype html>
<html>
<head>
	<meta charset="utf-8">
	
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<link rel="stylesheet" href="../css/foundation.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
	<link rel="icon" href="/favicon.png">
	<link href='https://fonts.googleapis.com/css?family=Fontdiner+Swanky' rel='stylesheet' type='text/css'>
	<script src="../js/vendor/modernizr.js"></script>
	<style>
	.LoggedIn {
		display: none;
	}
	.LoggedIn-Hide {
		display: none;
	}
	.EmailHid {
		color: red;
	}
	#profileImage {
		display: inline;
	}
	#title {
		display: inline;
	}
	.info {
		position: relative;
		display: block;
		left: 50%;
	}
	#idnum {
		display: inline;
	}
	</style>
	<title id='titleTop'>Members | Social</title>
<?php

include("../config.php");
$f3 = require('../fatfree/lib/base.php');
function jsLog($text) {
	echo "<script>console.log('" . $text . "');</script>";
}
if ($_SESSION["state"] == "true") {

	echo "<script>window.onload = function() {
		document.getElementById(\"profileWelcome\").innerHTML = '" . $_SESSION["user"] . "';
		document.getElementById(\"profileButton\").style.display = 'block'
		document.getElementById('loginButton').style.display = 'none';
	document.getElementById('registerButton').style.display = 'none';
	
	}</script>";
}else{
	echo "<script>window.onload = function() {
		document.getElementById(\"profileButton\").style.display = 'none'
	document.getElementById('loginButton').style.display = 'block';
	document.getElementById('registerButton').style.display = 'block';

	}</script>";
}
$listUser['name'] = $_GET['user'];
$listUser['name'] = preg_replace('/[^a-zA-Z0-9 -]+/', '', $listUser['name']);
$listUser['name'] = str_replace(' ', '-', $listUser['name']);
trim($listUser['name'], '-');
if (isset($listUser['name']) == false) {
	if($_SESSION["state"] == "true") {
		//$listUser = $_SESSION["user"];
		header("Location: ../u?user=" . $_SESSION['user']);
		die('Redirecting');
	}else{
		die('User does not exist');
	}
}else{
	echo "<script>document.getElementById('titleTop').innerHTML = '" . $listUser['name'] . " | Social';</script>";
}
//jsLog($listUser['name']);
$rows=$db->exec("SELECT ID,username,email,fullname,messages,EmailVisible FROM `social_users` WHERE username='" . $listUser['name'] . "'");
	//	jsLog(count($rows));
		if (count($rows) > 0) {
			foreach($rows as $row)
			$listUser['ID'] = $row['ID'];
			$listUser['EmailVisible'] = $row['EmailVisible'];
			$listUser['fullname'] = $row['fullname'];
			$listUser['email'] = $row['email'];
			$listUser['LastLogin'] = $row['LastLogin'];
			$listUser['msgs'] = $row['messages'];
		}else{
			echo "<script>window.onload = function() { document.getElementById('welcome').innerHTML = '<h1 class=\'text-center\'>User not found</h1>'; }</script>";
			//die();
		}
?>
</head>
<body>
<div class="top-bar">
  <div class="top-bar-left">
    <ul class="dropdown menu" data-dropdown-menu>
      <li class="menu-text">Social</li>
      <li>
        <a href="../">Home</a>
      </li>
	  <li><a href='../members.php'>Members</a></li>
	  <li><a href='rules.php'>Rules</a></li>
	  <li><a href='contact.php'>Support</a></li>
	  <li><a href='admin.php'>Admin</a></li>
    </ul>
  </div>
  <div class="top-bar-right">
    <ul class="menu">
		<li><a style="display:none" id='profileButton' class="LoggedIn" href="profile.php"><i class="fa fa-user" aria-hidden="true"></i> <span id='profileWelcome'></span></a></li>
		<li><a style="display:none" id='loginButton' href="login.php?mode=login">Login</a></li>
		<li><a style="display:none" id='registerButton' href="login.php?mode=register">Register</a></li>

    </ul>
  </div>
</div>
<br>
<br>
<div class="row">
	<div  id='welcome' class="large-12 callout ">
		<?php
			
		?>
		<div class="row">
			<div class='small-2 column'>
				<img id='profileImage' width="200px" height="200px" src='https://placeholdit.imgix.net/~text?txtsize=66&txt=?&w=200&h=200' />
			</div>
			<div class="large-10 column">
		<?php
			echo "<h1 id='title'>" . $listUser['name'] ." </h1> <span id='idnum'>ID #" . $listUser['ID'] . "</span><br>";
			if ($listUser['EmailVisible'] == 1) {
				echo "<span id='info'><strong>Email: </strong>" . $listUser['email'] . "</span>";
			}else{
				echo "<span id='info'><strong>Email: </strong>Hidden</span><br>";
			}
			echo "<br><span id='info'><strong>Last Login: </strong></span>";
		?>
		</div>
		</div>
	</div>
	<p class="text-center">Copyright © <a href="https://jackzmc.me">Jackz Minecraft</a>&nbsp;&nbsp;&nbsp;<a href="termsofservice.php">Terms of Service</a></p>
</div>

<script src="../js/vendor/jquery.js"></script>
<script src="../js/vendor/fastclick.js"></script>
<script src="../js/foundation.min.js"></script>

</body>
</html>