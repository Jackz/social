<?php
session_name("jackzmcSocial");
session_start();
include("config.php");
include("swear.php");
$f3 = require('fatfree/lib/base.php');
$db=new \DB\SQL('mysql:host=localhost;port=3306;dbname=' . $db_name,$db_user,$db_pass);
?> 
 <!doctype html>
<html>
<head>
	<meta charset="utf-8">
	
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<link rel="stylesheet" href="css/foundation.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
	<link rel="icon" href="/favicon.png">
	<link href='https://fonts.googleapis.com/css?family=Fontdiner+Swanky' rel='stylesheet' type='text/css'>
	<script src="js/vendor/modernizr.js"></script>
	<style>
	.EmailHid {
		color: red;
	}
	#profileImage {
		display: inline;
	}
	#title {
		display: inline;
	}
	.info {
		position: relative;
		display: block;
		left: 50%;
	}
	#idnum {
		display: inline;
	}
	.pm {
		position: absolute;
		top: 10px;
		right:10px;
		display: inline-block;
		float: right;
		
	}
	.bio {
		position: absolute;
		right: 300px;
		top: 40px;
	}
	.creator {
		position: absolute;
		display: inline-block;
		top:10px;
	}
	.message {
		position: relative;
		top: 5px;
	}
	.report {
		position: absolute;
		color: red;
		right:5px;
		bottom:5px;
	}
	.report:hover {
		color: darkred;
	}
	.like {
		position: absolute;
		color: #00a3cc;
		right:70px;
		bottom:7px;
	}
	.liked {
		position: absolute;
		color: blue;
		right:70px;
		bottom:7px;
	}
	.liked:hover {
		
		color: blue;

	}
	#rank {
		font-size:20px;
		color: gold;
	}
	</style>
	<title id='titleTop'>| Social</title>
<?php
$listUser['name'] = $_GET['user'];
$listUser['name'] = preg_replace('/[^a-zA-Z0-9 -]+/', '', $listUser['name']);
$listUser['name'] = str_replace(' ', '-', $listUser['name']);
trim($listUser['name'], '-');
if ($_SESSION["state"] == "true") {
	?>
	<script>window.onload = function() {
		document.getElementById("profileWelcome").innerHTML = '<?php echo $_SESSION["user"]; ?>';
		document.getElementById("profileButton").style.display = 'block';
		document.getElementById('loginButton').style.display = 'none';
	document.getElementById('registerButton').style.display = 'none';
	
	}</script>
	<?php
	if ($_SESSION['user'] == $listUser['name']) {
		$userNotMatch = false;
		
		//User does not match (false -> does match)
	}else{
		$userNotMatch = true;
	}
	$sessionData=$db->exec("SELECT swearFilter FROM `social_Users` WHERE username='" . $_SESSION['user'] . "'");
	if(count($sessionData) > 0) {
		foreach($sessionData as $sdata)
		$swearfilter = $sdata['swearFilter'];
	}else{
		$swearfilter = 1;
	}
}else{
	$userNotMatch = true;
	$swearfilter = 1;
	?>
	<script>
		document.getElementById("profileButton").style.display = 'none';
	document.getElementById('loginButton').style.display = 'block';
	document.getElementById('registerButton').style.display = 'block';

</script>
	<?php
}

if (isset($listUser['name']) == false) {
	if($_SESSION["state"] == "true") {
		//$listUser = $_SESSION["user"];
		header("Location: user.php?user=" . $_SESSION['user']);
		die('Redirecting');
	}else{
		die('User does not exist');
	}
}else{
	echo "<script>document.getElementById('titleTop').innerHTML = '" . $listUser['name'] . " | Social';</script>";
}
//jsLog($listUser['name']);
$rows=$db->exec("SELECT ID,username,email,fullname,messages,EmailVisible,realLastLogin,bio,swearFilter,rank FROM `social_users` WHERE username='" . $listUser['name'] . "'");
	//	jsLog(count($rows));
		if (count($rows) > 0) {
			foreach($rows as $row)
			$listUser['ID'] = $row['ID'];
			$listUser['bio'] = $row['bio'];
			if ($row['rank'] == "admin") {
				$rank = "<a id='rank' title='Administrator' onClick=\"alert('This user is an admin.');\"> <i  class=\"fa fa-star\" aria-hidden=\"true\"></i> <strong>Admin</strong></a>";
			}
			$listUser['EmailVisible'] = $row['EmailVisible'];
			$listUser['messages'] = $row['messages'];
			$listUser['swearFilter'] = $row['swearFilter'];
			$listUser['fullname'] = $row['fullname'];
			$listUser['email'] = $row['email'];
			$listUser['LastLogin'] = $row['LastLogin'];
			$listUser['msgs'] = $row['messages'];
			$listUser['avatarSet'] = $row['avatarSet'];
			$listUser['realLastLogin'] = $row['realLastLogin'];
			if(isset($listUser['bio']) == false || empty($listUser['bio']) == true) {
				echo "<script>window.onload = function() { document.getElementsByClassName('bio')[0].style.display = 'none'; }</script>";
			}else {
				$listUser['bio'] = wordwrap($listUser['bio'], 50, "<br>");
			}
		}else{
			echo "<script>window.onload = function() { document.getElementById('welcome').innerHTML = '<h1 class=\'text-center\'>User not found</h1>'; }</script>";
			//die();
		}
?>
</head>
<body>
<div class="reveal" id="pmBox" data-reveal>
	<h1>Private Message</h1>
	<p>Please specify who to send to.</p>
	<form method="post" action="sendPM.php">
	<input type='text' name="toSend" id='toSend' placeholder='Who should this go to?' />
	<textarea  rows="10" name="msg" id="msg" placeholder="Your message" ></textarea>
	<input type="submit" class="button success" id="submit" name="submit" />
	</form>
  <button class="close-button" data-close aria-label="Close modal" type="button">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
<div class="top-bar">
  <div class="top-bar-left">
    <ul class="dropdown menu" data-dropdown-menu>
      <li class="menu-text">Social</li>
      <li>
        <a href="../social">Home</a>
      </li>
	  <li><a href='members.php'>Members</a></li>
	  <li><a href='rules.php'>Rules</a></li>
	  <li><a href='contact.php'>Support</a></li>

    </ul>
  </div>
  <div class="top-bar-right">
    <ul class="menu">
		<li><a style="display:none" id='profileButton' class="LoggedIn" href="profile.php"><i class="fa fa-user" aria-hidden="true"></i> <span id='profileWelcome'></span></a></li>
		<li><a style="display:none" id='loginButton' href="login.php?mode=login">Login</a></li>
		<li><a style="display:none" id='registerButton' href="login.php?mode=register">Register</a></li>

    </ul>
  </div>
</div>
<br>
<br>
<div class="reveal" id="report" data-reveal>
	<span style="display:none" id='report_signin'>
	<h1>Report a post</h1>
	<p>You clicked to report Post #<span id='report_id'></span>. Please specify the message's content and why you have flagged it.<p>
	<form method="POST" action="flag.php">
		<label>Username</label><input type="text" id='username' DISABLED name='username' value='<?php echo $_SESSION["user"]; ?>'>
		<input type="hidden" id="messageID" name="messageID" value="" />
		<label>Message content</label><textarea placeholder="Please enter the message's content (what did it say)" id='content' name='content'></textarea>
		<label>Reasoning</label><textarea style="height:188px;" placeholder="Why did you want to flag this message? Specify here." id='reason' name='reason'></textarea>
		<input type="submit" class="button text-center" id="submit" name="submit" value="Report Message" />
	</form>
	<!-- MESSAGE HERE -->
	<!-- form here asking why -->
	<!--<form method="post" action="msgSend.php">
	<textarea type='text' name="composeText" id='composeText' placeholder='Put your message here' ></textarea>

	<input type="submit" class='button' name="submit" id='submit' value="Compose" />
	</form>-->
	</span>
	<span style="display:none" id='report_tologin'><h1>Report a post</h1><p>You need to be signed in to report a post. Sign in below: <br>
	<iframe height="365px" width="100%" frameborder="0" src='login_box.php?mode=login' ></iframe></span>
  <button class="close-button" data-close aria-label="Close modal" type="button">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
<div class="row">
	<div  id='welcome' class="large-12 callout primary">
		
		<div class="row">
			<div class='small-2 column'>
			<?php
			$avatarEmail = md5(strtolower(trim($listUser['email'])));
			$url = "https://www.gravatar.com/avatar/" . $avatarEmail . "?d=" . $defaultAvatar . "&r=pg&s=200";
			?>
				<img id='profileImage' width="200px" height="200px" src='<?php echo $url; ?>' />
			</div>
			<div class="large-10 column">
		<?php
			echo "<h1 id='title'>" . $listUser['name'] ." </h1>" . $rank . " <span id='idnum'>ID #" . $listUser['ID'] . "</span><br>";
			if ($listUser['EmailVisible'] == 1) {
				echo "<span id='info'><strong>Email: </strong>" . $listUser['email'] . "</span>";
			}else{
				echo "<span id='info'><strong>Email: </strong>Hidden</span><br>";
			}
			echo "<br><span ><strong>Last Login: </strong>" . $listUser['realLastLogin'] . "</span>";
			echo "<br><span ><strong>Messages: </strong>" . $listUser['messages'] . "</span>";
			if ($_SESSION['state'] == true):
		?>
			<span class="pm"><a data-open="pmBox" onClick="pmUser();">Send a private message</a></span>
		<?php endif; ?>
			<span class="bio"> "<?php echo $listUser['bio']; ?>"</span>
			</div>
		</div>
	</div>
	<?php
		
		
		$contents=$db->exec("SELECT * FROM `social_content` WHERE creator='" . $listUser['name'] . "'");
		if (count($contents) > 0) {
			foreach($contents as $content) {
				$msg = $content['message'];
				$msg = html_entity_decode($msg,ENT_QUOTES);
				if ($content['rank'] == "admin") {
					$atname = $content['creator'] . "<a style='color:gold' title='Administrator' onClick=\"alert('This user is an admin.');\"> <i  class=\"fa fa-star\" aria-hidden=\"true\"></i></a>";
				}else{
					$atname = $content['creator'];
				}
				if ($swearfilter == 1) {
						$msg = str_ireplace($swear, "**** ", $msg);
						//value (1 / 0)
	
				}else{
					//Filter: active
					$msg = str_ireplace($swear, "**** ", $msg);
				}
				if ($_SESSION['state'] == true && $userNotMatch == true) {
					$like = "<a id='like" . $content['id'] . "' class='like' title='Like this post' onClick='like(" . $content['id'] . ")'><i class=\"fa fa-thumbs-up\" aria-hidden=\"true\"></i> <span id='likeCount" . $content['id'] . "'>" . $content['likes'] . "</span></a>";
				}else{
					$like = "";
				}
				echo "<div class='large-12 callout'><a href='user.php?user=" . $content['creator'] . "'><img width='75px' height='75px' src='" . $url . "' /></a>&nbsp;&nbsp;<h4 class='creator'>By <a href='user.php?user=" . $content['creator'] . "'>@" . $atname . "</a> on " . $content['posted'] . "</h4><span class='message'>" . $msg . "</span>" . $like;
				if ($userNotMatch == true) {
					?>
					<a class='report' data-open='report' title='Report this post' onClick='report('<?php echo $content['id']; ?>')'><i class="fa fa-flag" aria-hidden="true"></i> Flag</a></div>
					<?php
				}else{
					echo "</div>";
				}
				//unset($atname);
			}
		}else{
			echo "<div class='callout alert text-center'><strong>Failed to grab content. Database returned nothing, sorry!</strong></div>";
		}
		?>
	<p class="text-center">Copyright © <a href="https://jackzmc.me">Jackz Minecraft</a>&nbsp;&nbsp;&nbsp;<a href="termsofservice.php">Terms of Service</a></p>
</div>

<script src="js/vendor/jquery.js"></script>
<script src="js/vendor/fastclick.js"></script>
<script src="js/foundation.min.js"></script>
<script>
$(document).foundation();

function pmUser() {
	document.getElementById("toSend").value = "<?php echo $listUser['name']; ?>"
}
$(document).foundation();
function report(id) {
	document.getElementById('report_id').innerHTML = id;
	document.getElementById('messageID').value = id;
	//alert("Not available");
}
function like(id) {
	
			// document.getElementById('likeCount' + id).innerHTML = parseInt(document.getElementById('likeCount' + id).innerHTML) + 1; //to remove
	$(function(){
       $.get("likePost.php?id=" + id + "&user=<?php echo $_SESSION['user']; ?>", function(data){
			//console.log("LikePost Response: " + data);
			
			var res = data.split("|");
			if (res[0] == "Success") {
				console.log('Like Post #' + res[2] + ', was Success');
				document.getElementById('like' + id).className = "liked";
				document.getElementById('likeCount' + id).innerHTML = res[1];
			}
       });
    });
	
	
	//console.log("Liked post #" + id) 
}

</script>
</body>
</html>