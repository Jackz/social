<?php
session_name("jackzmcSocial");
session_start();
?> 
 <!doctype html>
<html>
<head>
	<meta charset="utf-8">
	
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<link rel="stylesheet" href="css/foundation.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
	<link rel="icon" href="/favicon.png">
	<link href='https://fonts.googleapis.com/css?family=Fontdiner+Swanky' rel='stylesheet' type='text/css'>
	<script src="js/vendor/modernizr.js"></script>
	<style>
	.EmailHid {
		color: red;
	}
	</style>
	<title id='titleTop'>Members | Social</title>
</head>
<?php
/*$_SESSION["state"] = $_GET["loggedin"];
$_SESSION["user"] = $_GET["user"];
if ($_SESSION["state"] !== "true" && $_SESSION["state"] !== "false") {
	header("Refresh:0; url=?loggedin=false");
}
if (empty($_SESSION["user"]) == true) {
	$_SESSION["user"] = -1;
}

if ($_SESSION["state"] == "true") {
	if ($_SESSION["user"] == "-1") {
		//$_SESSION["state"] = "false";
		header("Refresh:0; url=?loggedin=false&user=");
	}
}
*/
/* dev code */
//this code will autologin if username set, state equal false
/*
else{
	if ($_SESSION["user"] !== -1) {
		header("Refresh:0; url=?loggedin=true&user=" . $_SESSION["user"]);
	}
}
*/
/* end dev code */
include("config.php");
$f3 = require('fatfree/lib/base.php');
$db=new \DB\SQL('mysql:host=localhost;port=3306;dbname=' . $db_name,$db_user,$db_pass);
if ($_SESSION["state"] == "true") {

	echo "<script>window.onload = function() {
		document.getElementById(\"profileWelcome\").innerHTML = '" . $_SESSION["user"] . "';
		document.getElementById(\"profileButton\").style.display = 'block'
		document.getElementById('loginButton').style.display = 'none';
	document.getElementById('registerButton').style.display = 'none';
	}</script>";
}else{
	echo "<script>window.onload = function() {
		document.getElementById(\"profileButton\").style.display = 'none'
	document.getElementById('loginButton').style.display = 'block';
	document.getElementById('registerButton').style.display = 'block';
	}</script>";
}
?>
<body>
<div class="top-bar">
  <div class="top-bar-left">
    <ul class="dropdown menu" data-dropdown-menu>
      <li class="menu-text">Social</li>
      <li>
        <a href="../social">Home</a>
      </li>
	  <li class='active'><a href='members.php'>Members</a></li>
	  <li><a href='rules.php'>Rules</a></li>
	  <li><a href='contact.php'>Support</a></li>
	 
    </ul>
  </div>
  <div class="top-bar-right">
    <ul class="menu">
		<li><a style="display:none" id='profileButton' class="LoggedIn" href="profile.php"><i class="fa fa-user" aria-hidden="true"></i> <span id='profileWelcome'></span></a></li>
		<li><a style="display:none" id='loginButton' href="login.php?mode=login">Login</a></li>
		<li><a style="display:none" id='registerButton' href="login.php?mode=register">Register</a></li>

    </ul>
  </div>
</div>
<br>
<br>
<div class="row">
	<div  id='welcome' class="large-12 callout ">
	<h1 class="text-center">Member List</h1>
	<p class="text-center">Register today! A gold star means an admin</p>
	<table>
		<thead>
			<tr>
				<th width="5px">ID</th>
				<th width="20px">Avatar</th>
				<th>Name</th>
				<th>Email</th>
				<th>Date Registered</th>
				<th>Last Login</th>
			</tr>
		</thead>
		<tbody>
		<?php
			$rows=$db->exec("SELECT rank,ID,email,EmailVisible,LastLogin,username,activated,realLastLogin FROM `SOCIAL_users`");
			if (count($rows) > 0) {
			  foreach($rows as $row) {
				if ($row['activated'] == 1) {
					if ($row['EmailVisible'] == 1) {

						$email = "<a href='mailto:" . $row['email'] . "'>" . $row['email'] . "</a>";
					}else{
						$email = "<span class='EmailHid'>Email is hidden</span>";
					}
					if ($row['rank'] == "admin") {
						$prefix = "<a style='color:gold' title='Administrator' onClick=\"alert('This user is an admin.');\"> <i  class=\"fa fa-star\" aria-hidden=\"true\"></i></a> ";
					}else{
						$prefix = "";
					}
					$avatarEmail = md5(strtolower(trim($row['email'])));
					$url = "https://www.gravatar.com/avatar/" . $avatarEmail . "?d=" . $defaultAvatar . "&r=pg&s=200";
						$img = "<a href='user.php?user=" . $row['username'] . "'><img width='75px' height='75px' src='" . $url . "' /></a>";
					$ID = "<a href='user.php?user=" . $row['username'] . "'>#" . $row['ID'] . "</a>";
					$user = $prefix . "<a href='user.php?user=" . $row['username'] . "'>" . $row['username'] . "</a>";
					
					echo "<tr><td>" . $ID . "</td><td>" . $img . "</td><td>" . $user . "</td><td>" . $email . "</td><td>" . $row['LastLogin'] ."</td><td>" . $row['realLastLogin'] . "</td></tr>";
					unset($email);
					unset($ID);
					unset($user);
				}
			  }
			}else{
				die("A fatal error occurred and the username list could not be listed");
			}
		
		
		?>
		</tbody>
		</table>
	</div>
	<p class="text-center">Copyright © <a href="https://jackzmc.me">Jackz Minecraft</a>&nbsp;&nbsp;&nbsp;<a href="termsofservice.php">Terms of Service</a></p>
</div>

<script src="js/vendor/jquery.js"></script>
<script src="js/vendor/fastclick.js"></script>
<script src="js/foundation.min.js"></script>

</body>
</html>