<?php
session_name("jackzmcSocial");
session_start();

 //$debug = true;
include("config.php");
require 'mailer/PHPMailerAutoload.php';
//fatFree 
$f3 = require('fatfree/lib/base.php');
$db=new \DB\SQL('mysql:host=localhost;port=3306;dbname=' . $db_name,$db_user,$db_pass);
if ($config_active !== true) {
	die("<strong>The config could not be loaded and as a fallback this page is disabled. <br>Please contact the admins at me@jackzmc.me</strong>");
}
function error($msg) {
	echo "<script>window.onload = function() { error('" . $msg . "'); }</script>";
}
function errorEnd($msg) {
	echo "<script>window.onload = function() { errorEnd('" . $msg . "'); }</script>";
}
function success($msg) {
	echo "<script>window.onload = function() { success('" . $msg . "'); }</script>";
}
$user = $_POST["usernameIn"]; //to change to post

$email = $_POST['email'];
$emailConfirm = $_POST['emailConfirm'];
$fullname = $_POST['fullname'];
$pass = $_POST["passwordIn"]; //post
$pass =  htmlspecialchars($pass);
$pass = htmlentities($pass);
$passconfirm = $_POST['passConfirm'];
$passconfirm =  htmlspecialchars($passconfirm);
$passconfirm = htmlentities($passconfirm);
$phone = $_POST['phone'];
$submitButton = $_POST["submit"]; //post
//ERROR CODES
/*
0 = unknown error 
1 = username not set [done]
2 = password not set [done]
3 = email not set [done]
4 = full name not set
5 = terms not checked
6 = email does not match emailconfirm
7 = password does not match passwordconfirm 
8 = passcofnrim not set [done]
9 = email confirm not set [done]

*/
/*
if (empty($user) == true || isset($user) == false) {
	if ($debug == false) {
		if ($submitButton == "Register") {
			header("Location: login.php?mode=register");
			die();
		}else{
			header("Location: login.php?mode=login");
			die();
		}
		die("Username not set, reroute to login");
	}else{
		if ($submitButton == "Register") {
			print("Location: login.php?mode=register");
			die();
		}else{
			print("Location: login.php?mode=login");
			die();
		}
		die("Username not set, reroute to login");
		
	}
}
if (empty($pass) == false && isset($pass) == true) {
	if (empty($passconfirm) == true || isset($passconfirm) == false) {
		if ($debug == false) {
			if ($submitButton == "Register") {
				header("Location: login.php?mode=register&error=8");
				die();
			}else{
				header("Location: login.php?mode=login&error=8");
				die();
			}
			die("Passwordconfirm is not set");
		}else{
			if ($submitButton == "Register") {
				print("Location: login.php?mode=register&error=8");
				die();
			}else{
				print("Location: login.php?mode=login&error=8");
				die();
			}
		}
	}
}else{
	if ($debug == false) {
		if ($submitButton == "Register") {
			header("Location: login.php?mode=register&error=2");
			die();
		}else{
			header("Location: login.php?mode=login&error=2");
			die();
		}
		die("Password is not set");
	}else{
		if ($submitButton == "Register") {
			print("Location: login.php?mode=register&error=2");
			die();
		}else{
			print("Location: login.php?mode=login&error=2");
			die();
		}
	}
}
if (empty($email) == false && isset($email) == true) {
	if (empty($emailConfirm) == true || isset($emailConfirm) == false) {
		if ($debug == false) {
			if ($submitButton == "Register") {
				header("Location: login.php?mode=register&error=9");
				die();
			}else{
				header("Location: login.php?mode=login&error=9");
				die();
			}
			die("emailwordconfirm is not set");
		}else{
			if ($submitButton == "Register") {
				print("Location: login.php?mode=register&error=9");
				die();
			}else{
				print("Location: login.php?mode=login&error=9");
				die();
			}
		}
	}else{
		//check compare
		if ($email !== $emailConfirm) {
			header("Location: login.php?mode=register");
		}
	}
}else{
	if ($debug == false) {
		if ($submitButton == "Register") {
			header("Location: login.php?mode=register&error=3");
			die();
		}else{
			header("Location: login.php?mode=login&error=3");
			die();
		}
		die("emailword is not set");
	}else{
		if ($submitButton == "Register") {
			print("Location: login.php?mode=register&error=3");
			die();
			
		}else{
			print("Location: login.php?mode=login&error=3");
			die();
		}
	}
}
if (empty($fullname) == false || isset($fullname) == true) {
	
}*/
/*CHECK VALIDATION 
*/


//POST




//email
$email = strtolower($email);
$email = preg_replace('/[^a-z@.A-Z0-9 -]+/', '', $email);
$email = str_replace(' ', '-', $email);
trim($email, '-');

$emailConfirm = strtolower($emailConfirm);
$emailConfirm = preg_replace('/[^a-z@.A-Z0-9 -]+/', '', $emailConfirm);
$emailConfirm = str_replace(' ', '-', $emailConfirm);
trim($emailConfirm, '-');
//full name
$fullname = preg_replace('/[^a-zA-Z0-9 -]+/', '', $fullname);
$fullname = str_replace(' ', '-', $fullname);
trim($fullname, '-');

//username
$user = preg_replace('/[^a-zA-Z0-9 -]+/', '', $user);
$user = str_replace(' ', '-', $user);
trim($user, '-');
$safeuser = strtolower($user);


//
$realLast = date("F j, Y") . " @ " . date("h:i A ") . " CST";
$user = ucfirst(strtolower($user));

//$pass = hash("sha512",hash("sha512",$pass));
//$pass = "disabled";

//$_SESSION["user"] = $user;
//$_SESSION["name"] = $_GET["name"]; 
//NEED TO CHECK IF BLANK, DO NOT RELY ON HTML5
//$_SESSION["email"] = $_GET["email"];
if(preg_match("/^[0-9]{3}-[0-9]{4}-[0-9]{4}$/", $phone)) {
}else{
	errorEnd("Your phone number is invalid. Must be in 000-000-0000 format. If you are having trouble, please contact admins and register with phone number '000-000-0000'");
	$failed = true;
}
if ($debug == true) {
	echo "user: " . $user . "<br>";
	echo "name: " . $email . "<br>"; 
	echo "pass(hash): " . $pass . "<br>";
	echo "email: " . $_POST["email"] . "<br>";
	echo "submit: " .$_POST["submit"] . "<br>";
}
if (isset($submitButton) == true) {
	if ($submitButton == "Login") {
		$rows=$db->exec("SELECT username,password,rank FROM `social_Users` WHERE safeUsername='" . $safeuser . "'");
		
		if (count($rows) > 0) {
			foreach($rows as $row)
			
			if (empty($row['password']) == true ) {
				errorEnd("Password is blank. Click <a href='resetpassform.php'>here to reset</a>");
				$failed = true;
				//die();
			}
				
			if (password_verify($pass, $row['password']) ) {
				
				unset($pass);
				if ($debug == false) {
				$_SESSION["user"] = $user;
				$_SESSION["rank"] = $row['rank'];
				$_SESSION["state"] = true;
				//$_SESSION["name"];
					$rows=$db->exec("UPDATE `social_Users` SET `realLastLogin`='" . $realLast . "' WHERE safeUsername='" . $safeuser . "'");
					header("Location: ../social/profile.php");
				}
			}else {
				$failed = true;

				errorEnd("Password or username is incorrect. Sowwy ;(");
				unset($pass);
			}
			
		}else{
			$failed = true;
			errorEnd("Database error occurred, and could not check your information. Try again later or contact support ;(");
				}
				/*if ($debug == false) {
		header("Location: ../social/profile.php");
				}*/
	}else if ($submitButton == "Register") {
		$options = [
			'cost' => 11,
			'salt' => mcrypt_create_iv(22, MCRYPT_DEV_URANDOM),
		];
		$pass = password_hash($pass, PASSWORD_BCRYPT, $options)."\n";
				$key = md5(uniqid(rand(), true));
				//$db->exec("INSERT INTO `social_users` (`username`, `password`, `fullname`, `email`) VALUES ('" . $user . "',)"); // outputs 4
				$rows=$db->exec("SELECT id,username,email FROM `social_Users` WHERE username='" . $user . "'");
				// OR email='" . $email . '''
				//echo count($rows) . "<br>";
				if (count($rows) > 0) {
		die("<strong>already exists</strong>");
				}else{
		$mail = new PHPMailer;
		
			//$mail->SMTPDebug = 3;                               // Enable verbose debug output
			//$mail->SMTPDebug = 1;
			$mail->isSMTP();                                      // Set mailer to use SMTP
			$mail->Host = 'smtp.zoho.com';  // Specify main and backup SMTP servers
			$mail->SMTPAuth = true;                               // Enable SMTP authentication
			$mail->Username = 'support@jackzcraft.net';                 // SMTP username
			$mail->Password = 'jackzcraft7';                           // SMTP password
			$mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
			$mail->Port = 465;                                    // TCP port to connect to
		
			$mail->setFrom('support@jackzcraft.net', 'Validation');
			$mail->addAddress($email, $fullname);     // Add a recipient
			//$mail->addAddress('ellen@example.com');               // Name is optional
			//$mail->addReplyTo('info@example.com', 'Information');
			//$mail->addCC('cc@example.com');
			//$mail->addBCC('bcc@example.com');
		
			//$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
			//$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
			$mail->isHTML(true);                                  // Set email format to HTML
		
			$mail->Subject = 'Verify your account';
			$mail->Body    = 'Hello ' . $user . ". You recently signed up for Social. To complete your registeration please confirm your email by clicking the link below. If you did not register, please disregard this email. <br><br><a href='https://code.jackzmc.me/social/activate.php?email=" . $email . "&key=" . $key . "'>Click here to confirm";
				//$mail->AltBody = '';

				if(!$mail->send()) {
					error( 'Message could not be sent.');
					errorEnd( 'Mailer Error: <br>' . $mail->ErrorInfo);
				} else {
					error( 'Message has been sent');
				}
				
				
			$db->exec('INSERT INTO `social_users` (`username`, `password`, `fullname`, `email`, `messages`, `EmailVisible`, `actKey`, `activated`,`LastLogin`, `swearFilter`,`realLastLogin`,`phonenum`,`safeUsername`) VALUES (:user,:pass,:fullname,:email,0,true,:key,false,:lastLogin,false,:realLastLogin,:phonenum,:safeUsername)',array(':user'=>$user,':pass'=>$pass,':fullname'=>$fullname,':email'=>$email,':key'=>$key,':lastLogin'=>date("F j, Y"),':realLastLogin'=>date("F j, Y"),':phonenum'=>$phone,':safeUsername'=>$safeuser));
			//$rows=$db->exec('SELECT id,username,email FROM `social_Users` WHERE username=\'' . $user . '\' OR email=\'' . $_GET['email'] . '\'');
			unset($pass);
		
				

				
			success("<strong>Registered you into database. " . $id . "</strong><br><strong>We have sent an email to </strong>" . $email . "<br><strong><a href='../social/profile.php'>(We actually didn't, click here to login)</a></strong><br>Sending emails will not activate. Click above, your already activated!");
		}
	}else{
		errorEnd("<strong>An error has occurred, unknown mode was set</strong><br>");
	}
}else{
	errorEnd("<strong>An error has occurred, please do not load this page directly.");
}

?>
<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<link rel="stylesheet" href="css/foundation.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
	<link rel="icon" href="/favicon.png">
	<link href='https://fonts.googleapis.com/css?family=Fontdiner+Swanky' rel='stylesheet' type='text/css'>
	<script src="js/vendor/modernizr.js"></script>
	<style>
	.LoggedIn {
		display: none;
	}
	.LoggedIn-Hide {
		display: none;
	}
	</style>
	<title id='titleTop'>Validation</title>
</head>


<body>
<br><br>
<h1 class="text-center" id='title'></h1>
<div class="text-center" style="display:none" class="row" id='errorContainer'>
<div class="large-12 callout alert" id='error'></div>
<div class="text-center" style="display:none" class="row" id='successContainer'>
<div class="large-12 callout success" id='success'></div>
</div>


<script>
function error(message) {
	document.getElementById('error').innerHTML += message;
}
function errorEnd(message) {
	document.getElementById('title').innerHTML = "Validation Error";
	document.getElementById('errorContainer').style.display = "block";
	document.getElementById('error').innerHTML = message + "<br><a href='login.php' class='button'>Return to login</a>";
}
function success(msg) {
	 document.getElementById('successContainer').style.display = "block";
	document.getElementById('success').innerHTML = msg + "<br><a href='login.php' class='button'>Return to login</a>";
}

</script>
</body>

</html>