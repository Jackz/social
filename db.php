<?php 
// If you installed via composer, just use this code to requrie autoloader on the top of your projects.
require 'vendor/autoload.php';
 
// Or if you just download the medoo.php into directory, require it with the correct path.
require  'medoo.php';
 
$database = new medoo([
	'database_type' => 'sqlite',
	'database_file' => 'C:/Apache24/vhosts/code/social/database.db'
]);
 
$database->insert("account", [
	"user_name" => "foo",
	"email" => "foo@bar.com"
]);
 ?>