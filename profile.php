<?php
session_name("jackzmcSocial");
session_start();
include("swear.php");

include("config.php");
$f3 = require('fatfree/lib/base.php');
$db=new \DB\SQL('mysql:host=localhost;port=3306;dbname=' . $db_name,$db_user,$db_pass);
if ($config_active !== true) {
	die("<strong>The config could not be loaded and as a fallback this page is disabled. <br>Please contact the admins at me@jackzmc.me</strong>");
}
if ($_SESSION["state"] === true) {

	echo "<script>window.onload = function() {
		document.getElementById(\"profileWelcome\").innerHTML = '" . $_SESSION["user"] . "';
		document.getElementById(\"profileButton\").style.display = 'block'
		document.getElementById('loginButton').style.display = 'none';
	document.getElementById('registerButton').style.display = 'none';
	
	}</script>";
	$rows=$db->exec("SELECT id,rank,username,email,fullname,messages,LastLogin,emailVisible,swearFilter,realLastLogin,bio,phonenum FROM `social_Users` WHERE username='" . $_SESSION['user'] . "'");
		if (count($rows) > 0) {
			foreach($rows as $row)
			$PROFILE['bio'] = $row['bio'];
			$PROFILE['fullname'] = $row['fullname'];
			$PROFILE['email'] = $row['email'];
			$PROFILE['LastLogin'] = $row['LastLogin'];
			$PROFILE['msgs'] = $row['messages'];
			if($row['rank'] == "admin") {
				$PROFILE['rankIcon'] = "<a style='color:gold' title='Administrator' onClick=\"alert('This user is an admin.');\"> <i  class=\"fa fa-star\" aria-hidden=\"true\"></i></a>";
			}
			$PROFILE['emailVisible'] = $row['emailVisible'];
			$PROFILE['swearFilter'] = $row['swearFilter'];
			$PROFILE['phonenum'] = $row['phonenum'];
			if(isset($PROFILE['bio']) && !empty($PROFILE['bio'])) {
				$PROFILE['bio'] = "<strong>BIO: </strong>\"" . $PROFILE['bio'] . "\" (<a data-open='setBio'>change</a>)";
				$PROFILE['editBioSet'] = $row['bio'];
			}else{
				$PROFILE['bio'] = "<strong>No bio is set, would you like to <a data-open='setBio'>create one?</a></strong>";
			}
			$PROFILE['realLastLogin'] = $row['realLastLogin'];
		}else{
			header("Location: login.php?mode=register");
		}
	//$profile['fullname'] = "";
	//$profile['email'] = "";
}else{
	header("Location: login.php?mode=login");
	/*echo "<script>window.onload = function() {
		document.getElementById(\"profileButton\").style.display = 'none'
	document.getElementById('loginButton').style.display = 'block';
	document.getElementById('registerButton').style.display = 'block';
	}</script>";*/
}

?>
 <!doctype html>
<html>
<head>
	<meta charset="utf-8">
	
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<link rel="stylesheet" href="css/foundation.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
	<link rel="icon" href="/favicon.png">
	<link href='https://fonts.googleapis.com/css?family=Fontdiner+Swanky' rel='stylesheet' type='text/css'>
	<script src="js/vendor/modernizr.js"></script>
	<style>
/*	.bio {
		position: absolute;
		top:175px;
		right:300px;
	}*/
	.creator {
		position: absolute;
		display: inline-block;
		top:10px;
	}
	.message {
		position: relative;
		top: 5px;
	}
	.report {
		position: absolute;
		color: red;
		right:5px;
		bottom:5px;
	}
	.report:hover {
		color: darkred;
	}
	.like {
		position: absolute;
		color: #00a3cc;
		right:70px;
		bottom:7px;
	}
	.liked {
		position: absolute;
		color: blue;
		right:70px;
		bottom:7px;
	}
	.liked:hover {
		
		color: blue;

	}
	#ResetPass {
		display: inline-block;
		position: absolute;
		right:100px;
		bottom:0px;
	}
	#logout {
		display: inline-block;
		position: absolute;
		bottom:16px;
		right:10px;
		

	}
	
	</style>
	<title id='titleTop'>Profile | Social</title>
</head>


<body>
<div class="reveal" id="setBio" data-reveal>
	<h1>BIO</h1>
	<p>Describe yourself! What do you do? Your job, your life?</p>
	<form method="post" action="updateBio.php">
	<textarea rows="6" id='TheBio' name='TheBio' placeholder='Describe yourself! What do you do? Your job, your life'><?php echo $PROFILE['editBioSet']; ?></textarea>
	<input type="submit" class="button" id="submit" />
	</form>
  <button class="close-button" data-close aria-label="Close modal" type="button">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
<div class="reveal" id="emailChange" data-reveal>
	<h1>Email Change</h1>
	<p>Type your new email to change to</p>
	<form method="post" action="changeEmail.php">
	<input type='email' name="changeEmailMain" id='changeEmailMain' placeholder='Enter your new email' />
	<input type="email" name="changeEmailConfirm" id="changeEmailConfirm" placeholder="Confirm your email" />
	<input type="submit" class='button' name="submit" id='submit' value="Change Email" />
	</form>
  <button class="close-button" data-close aria-label="Close modal" type="button">
    <span aria-hidden="true">&times;</span>
  </button>
</div>

<div class="reveal" id="passReset" data-reveal>
	<h1>Reset your password</h1>
	<p>Please type in your old password and new password</p>
	<form method="post" action="resetPassword.php">
	<input type='password' name="old" id='old' placeholder='Enter your old password' />
	<input type="password" name="newPass" id="newPass" placeholder="Confirm your email (8+ CHARACTERS)" />
	<input type="password" name="newPassConfirm" id="newPassConfirm" placeholder="Confirm your password (8+ CHARACTERS)" />
	<input type="submit" class='button' name="submit" id='submit' value="Update password" />
	</form>
  <button class="close-button" data-close aria-label="Close modal" type="button">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
<div class="top-bar">
  <div class="top-bar-left">
    <ul class="dropdown menu" data-dropdown-menu>
      <li class="menu-text">Social</li>
      <li>
        <a href="../social">Home</a>
      </li>
	  <li><a href='members.php'>Members</a></li>
	  <li><a href='rules.php'>Rules</a></li>
	  <li><a href='contact.php'>Support</a></li>
	 <?php if ($row['rank'] == "admin") { echo "<li><a href='admin'>Admin</a></li>"; } ?>
    </ul>
  </div>
  <div class="top-bar-right">
    <ul class="menu">
		<li class="active"><a style="display:none" id='profileButton' class="LoggedIn" href="profile.php"><i class="fa fa-user" aria-hidden="true"></i> <span id='profileWelcome'></span></a></li>
    </ul>
  </div>
</div>
<br>
<br>
<div class="row " data-equalizer>
	<!--<div class="large-12">
	
	
	<Br>
	<a class="button alert" href="logout.php">Logout</a>
	</div>-->

	 <div class="medium-2 columns">
		<ul class="tabs vertical" id="example-vert-tabs" data-tabs>
		  <li class="tabs-title is-active"><a href="#profile" aria-selected="true">Profile</a></li>
		  <li class="tabs-title"><a href="#settings">Settings</a></li>
		</ul>
	 </div>
  <div class="medium-7 columns ">
	<div class="tabs-content vertical callout primary" data-tabs-content="example-vert-tabs">
	  <div class="tabs-panel is-active" id="profile">

		<?php
			$avatarEmail = md5(strtolower(trim($PROFILE['email'])));
			$url = "https://www.gravatar.com/avatar/" . $avatarEmail . "?d=" . $defaultAvatar . "&r=pg&s=75";
			?>
			<img id='profileImage' width="75px" height="75px" src='<?php echo $url; ?>' />&nbsp;&nbsp;<h1 style="display:inline" id='profile_username'><?php echo $PROFILE['fullname']; ?></h1><h3 style='display:inline' class="subheader"> <?php echo $PROFILE['rankIcon']; ?>(<a href="user.php?user=<?php echo $_SESSION['user']; ?>">@<?php echo $_SESSION['user']; ?></a>)</h3>
			<br><br>
			<?php echo "<span class='bio'>" . $PROFILE['bio'] . "</span>"; ?>
			<hr>
			
		<label>Current Email </label><input style="width:30%;display:inline;" placeholder="Something happened..." type="text" id="accinfo_email" name="accinfo_email" readonly />
		<label>Phone number <a title="This is for two factor authenication. Read more in support page">What? (Hover)</a></label>
		<input style="width:30%;display:inline" type="text" readonly placeholder="No phone set" value="<?php echo $PROFILE['phonenum']; ?>" />&nbsp;<span>Contact staff to change</span>
		<br>
		<?php
		echo "<script>document.getElementById('accinfo_email').value = '" . $PROFILE["email"] . "'</script>";
		echo "<strong>User ID: </strong>#" . $row['id'];
	
		
		
		
		//echo "<br><strong>Email: </strong>" . $PROFILE["email"];
		//echo "<br><strong>Password: </strong> jk";
	?>
		<Br><a id="logout" class="button alert text-center" href="logout.php">Logout</a>
	</div>
	
	
	  <div class="tabs-panel" id="settings">
		<form method="POST" action="profileSettings.php">
		<label>Display username</label>
		<input type="text" id="pOption_username" name="pOption_username" value="<?php echo $_SESSION['user']; ?>" />
		</label>
		<label>Full name</label>
		<input type="text" id="pOption_fullname" name="pOption_fullname" value="<?php echo $PROFILE['fullname']; ?>" />
		<label>Password</label>
		<a class="button" data-open="passReset">Reset or change password</a>
		<label>Primary email</label>
		<input readonly type="text" id="pOption_email" name="pOption_email" value="<?php echo $PROFILE['email']; ?>" />
		<label>BIO - About yourself</label>
		<textarea id="pOption_bio" name="pOption_bio" rows="3"><?php echo $PROFILE['editBioSet']; ?></textarea>
		
		<input class="switch" type="checkbox" id="pOption_emailVis" name="pOption_emailVis" <?php if($PROFILE['emailVisible'] == true) { echo "checked"; } ?> readONLY value="emailvisible" />
		<label>Email Visibility (Can people see it)</label>
		<input class="switch" type="checkbox" id="pOption_swearfilter" <?php if($PROFILE['swearFilter'] == true) { echo "checked"; } ?> name="pOption_swearfilter" readonly value="swearfilter"/>
		<label>Swear Filter</label>
		<br><input id="profileOptionSave" type="submit" class="button success" value="Save" />
		</form>
	  </div>
	</div>
  </div>


	<div class="medium-3 column callout secondary" data-equalizer-watch>
		<h4 class="text-center"><u>Account Preferences</u></h4>
		<strong>Registered On </strong><?php echo $PROFILE['LastLogin']; ?><br>
		<strong>Last Login </strong><?php echo $PROFILE['realLastLogin']; ?><br>
		<strong>Messages Sent </strong><?php echo $PROFILE['msgs']; ?><br>
		<strong>Email Visibility </strong><?php if ($PROFILE['emailVisible'] == true) { echo "True"; }else{ echo "False"; }?><br>
		<strong>Swear Filter </strong><?php if ($PROFILE['swearFilter'] == true) { echo "True"; }else{ echo "False"; }?><br>
		<hr>
		<strong><u>Your avatar</u></strong>
		<p>To get your avatar icon we use Gravatar.com, sign up for the website and your icon on the site will be used. We use the primary email you registered with.</p>
	</div>
</div>
<hr>
<h4 class="text-center">Your messages</h4>
<div class="row">
	<?php
		$contents=$db->exec("SELECT * FROM `social_content` WHERE creator='" . $_SESSION['user'] . "'");
		if (count($contents) > 0) {
			foreach($contents as $content) {
					
					$msg = $content['message'];
					$msg = html_entity_decode($msg,ENT_QUOTES);
					if ($content['rank'] == "admin") {
						$atname = $content['creator'] . "<a style='color:gold' title='Administrator' onClick=\"alert('This user is an admin.');\"> <i  class=\"fa fa-star\" aria-hidden=\"true\"></i></a>";
					}else{
						$atname = $content['creator'];
					}
					if ($PROFILE['swearFilter'] == 1) {
						$msg = str_ireplace($swear, "**** ", $msg);
					}
					echo "<div class='large-12 callout'><a href='user.php?user=" . $content['creator'] . "'><img width='75px' height='75px' src='" . $url . "' /></a>&nbsp;&nbsp;<h4 class='creator'>By <a href='user.php?user=" . $content['creator'] . "'>@" . $atname . "</a> on " . $content['posted'] . "</h4><span class='message'>" . $msg . "</span></div>";
					//unset($atname);
			}
		}else{
			echo "<div class='callout alert text-center'><strong>No messages were found, why not post some?</strong></div>";
		}
	?>

</div>
<hr>
<div class="row">
	<div class="large-12 callout">
		<?php include('ideas.html') ?>
	</div>
</div>
	<p class="text-center">Copyright © <a href="https://jackzmc.me">Jackz Minecraft</a>&nbsp;&nbsp;&nbsp;<a href="termsofservice.php">Terms of Service</a></p>
	<!--<Div class="row">
	<div class="large-12 callout secondary text-center">
		Copyright © <a href="https://jackzmc.me">Jackz Minecraft</a>  <!-- remove br and space when ad --> <br>&nbsp;
		<!-- insert ad here -->
	<!--
	</div>
</div>-->

<script src="js/vendor/jquery.js"></script>
<script src="js/vendor/fastclick.js"></script>
<script src="js/foundation.min.js"></script>
<script>
$(document).foundation();


</script>
</body>
</html>